
#ifndef WEATHEROBJECT_H
#define WEATHEROBJECT_H

#include <QObject>

class WeatherObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString day READ day WRITE setDay NOTIFY dayChanged)
    Q_PROPERTY(QString weatherType READ weatherType WRITE setWeatherType NOTIFY weatherTypeChanged)
    Q_PROPERTY(QString temp READ temp WRITE setTemp NOTIFY tempChanged)
    Q_PROPERTY(QString windSpeed READ windSpeed WRITE setWindSpeed NOTIFY windSpeedChanged)
    Q_PROPERTY(QString precip READ precip WRITE setPrecip NOTIFY precipChanged)

public:
    WeatherObject(QObject *parent = 0);
    WeatherObject(const QString &day, const QString &weatherType, const QString &temp, const QString &windSpeed, const QString &precip, QObject *parent = 0);

    QString day() const;
    void setDay(const QString &day);

    QString weatherType() const;
    void setWeatherType(const QString &weatherType);

    QString temp() const;
    void setTemp(const QString &temp);

    QString windSpeed() const;
    void setWindSpeed(const QString &windSpeed);

    QString precip() const;
    void setPrecip(const QString &precip);

signals:
    void dayChanged();
    void weatherTypeChanged();
    void tempChanged();
    void windSpeedChanged();
    void precipChanged();

private:
    QString m_day;
    QString m_weather_type;
    QString m_temp;
    QString m_wind_speed;
    QString m_precip;
};

#endif // WEATHEROBJECT_H