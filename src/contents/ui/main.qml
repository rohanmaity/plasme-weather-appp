import QtQuick 2.6
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.5 as Controls
import QtQuick.Layouts 1.11

Kirigami.ApplicationWindow {
    id: root

    title: "Weather"

    Controls.SwipeView {
        id: view
        width: parent.width
        height: parent.height
        currentIndex: bar.currentIndex

        WeatherCards {
            id: pui
        }

        WeatherCards {
            id: op
        }

        WeatherCards {
            id: po
        }
    }

    header: Controls.TabBar {
        currentIndex: view.currentIndex
        id: bar
        width: parent.width
        Controls.TabButton {
            text: qsTr("Kolkata")
        }
        Controls.TabButton {
            text: qsTr("Banglore")
        }
        Controls.TabButton {
            text: qsTr("Japan")
        }
    }
}
