#include "weatherobject.h"

WeatherObject::WeatherObject(QObject *parent)
    : QObject(parent)
{
}

WeatherObject::WeatherObject(const QString &day, const QString &weatherType,const QString &temp,const QString &windSpeed,const QString &precip, QObject *parent)
    : QObject(parent),
      m_day(day),
      m_weather_type(weatherType),
      m_temp(temp),
      m_precip(precip),
      m_wind_speed(windSpeed)
{
}

QString WeatherObject::day() const
{
    return m_day;
}

void WeatherObject::setDay(const QString &day)
{
    if (day != m_day)
    {
        m_day = day;
        emit dayChanged();
    }
}

QString WeatherObject::weatherType() const
{
    return m_weather_type;
}

void WeatherObject::setWeatherType(const QString &weatherType)
{
    if (weatherType != m_weather_type)
    {
        m_weather_type = weatherType;
        emit weatherTypeChanged();
    }
}

QString WeatherObject::temp() const
{
    return m_temp;
}

void WeatherObject::setTemp(const QString &temp)
{
    if (temp != m_temp)
    {
        m_temp = temp;
        emit tempChanged();
    }
}

QString WeatherObject::windSpeed() const
{
    return m_wind_speed;
}

void WeatherObject::setWindSpeed(const QString &day)
{
    if (day != m_wind_speed)
    {
        m_day = day;
        emit windSpeedChanged();
    }
}

QString WeatherObject::precip() const
{
    return m_precip;
}

void WeatherObject::setPrecip(const QString &precip)
{
    if (precip != m_precip)
    {
        m_precip = precip;
        emit precipChanged();
    }
}