#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include "weatherobject.cpp"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("Weather");

    QQmlApplicationEngine engine;

    QList<QObject*> weatherList;
    weatherList.append(new WeatherObject("Friday","ThunderStorm","12 C","35 Km/h","65%"));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    if (engine.rootObjects().isEmpty())
    {
        return -1;
    }

    return app.exec();
}
